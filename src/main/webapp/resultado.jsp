<%-- 
    Document   : resultado
    Created on : 20-abr-2021, 23:47:24
    Author     : AOrellana
--%>

<%@page import="cl.modelo.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%

    Calculadora calculadora = (Calculadora) request.getAttribute("calculadora");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <label>Resultado es: <%=calculadora.getResultadoSuma()%></label>
    </body>
</html>
